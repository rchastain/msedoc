# MSEdoc

User supplied documentation for MSEide+MSEgui. 

## MSEgui HTML documentation

In [html](html) folder, you can find HTML documentation for MSEgui. There is two versions: one generated with fpdoc, the other with PasDoc.

## Martin's answers

In [text](text) folder, you can find Martin's answers on Sourceforge (and elsewhere).

## Online HTML documentation

The HTML documentation is available on [msegui.net](https://msegui.net/):

- [MSEgui online documentation (fpdoc)](https://msegui.net/doc/fpdoc/index.html)
- [MSEgui online documentation (pasdoc)](https://msegui.net/doc/pasdoc/index.html)

## Migration warning

Project continued [here](https://codeberg.org/rchastain/msedoc).
